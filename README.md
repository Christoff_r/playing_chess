# Chess :wheelchair:
![banner](https://emoji.slack-edge.com/T03SBHA7D8R/gigathink/ea14f6aa9c5692d4.gif)

![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-brightgreen)
![language](https://img.shields.io/badge/language-c%2B%2B-blue)
![contributors](https://img.shields.io/badge/contributors-2-orange)

This program allows two people to play chess against one another, in the `CLI`. It is a "hot seat" program, two players share one computer, takeing turns while playing. 

## Table of Contents

- [Background](#background)
- [Install](#install)
- [Usage](#usage)
- [Who made what](#who-made-what)
- [License](#license)


## Background

The motivation behinde this repository, where to cooporate on a project so we could learn how to use git with GitLab as source control, aswell as a way to put our newly acquired c++ knowlagde into use

This program let to users play chess against one another. See [usage](#usage) for more details.

## Install
Below is a short description on how to install the program along with some [dependencied](#dependencies).

### Dependencies

These dependencies are needed to compile and run the program properly.

#### Compiler

The repository comes with a CMakeLists.txt and can be compiled with [CMake](https://cmake.org/install/) if installed, or with g++ if you like to do it manualy.

#### UTF-8 Support

This is optional, if you terminal does not support UTF-8.

In order to see the symbols properly when printed out you can run the code in the [VS Code](https://code.visualstudio.com/Download) terminal.

If you want to use your Ubuntu terminal, you must make sure it support UTF-8, by following [these](https://unix.stackexchange.com/questions/303712/how-can-i-enable-utf-8-support-in-the-linux-console) steps (this did'nt work for me).


### Clone from GitLab

Navigate to were you want the repository to be. Open the `CLI` and type the following:

```
git clone https://gitlab.com/Christoff_r/playing_chess.git
```

## Usage

The program need to compile first.

### Compile

In the repository, open the `CLI` and type the following. 

#### Compile with Cmake

```
cd build
cmake ../
make
```
#### Compile with g++

```
cd build

g++ main.cpp inputHandler.cpp bishop.cpp chessBoard.cpp chessman.cpp gameState.cpp king.cpp knight.cpp mateCheckForCheckMate.cpp newGame.cpp pawn.cpp queen.cpp rook.cpp userCommands.cpp -o main
```

### Run

To run the program open the `CLI` in the prepository (if you are not still in the build folder from the last step) and type the following

```
cd build
./main
```
### Interact

To Start
Enter: new

Now a chessboard should be displayed

Player1 enters in: move

The Game will now ask for coordinates which are given by entering the coords of the piece and then the destination ex:

<img style = "float: non;" src="image-1.png">

Piece at 1,3 moves to the position 3,3

"Shows how to make a movement on a chessboard made in new"

<img style = "float: none;" src="image.png">


### List of commands
 
- help ~Shows a list of possible commands
- new ~Creates a new Chessboard ready to play
- load ~Loads chessman info
- save ~Saves chessman info
- ff ~Concedes match
- move ~Command needed to be called BEFORE you enter coordinates for the movement
- birb ~Ends reality or something

## Who made what

A list of who made what in this project.

### Christoffer
- newGame.cpp
- newGame.h
- chessBoard.cpp
- chessBoard.h
- gameState.cpp
- gameState.h
- inputHandler.cpp
- inputHandler.h
- userCommands.cpp
- userCommands.h
- README.md
- .gitignore

### Rasmus
- birb.cpp
- birb.h
- pawn.cpp
- pawn.h
- queen.cpp
- queen.h
- chessman.cpp
- chessman.h
- king.cpp
- king.h
- mateCheckForCheckMate (Not working)
- bishop.cpp
- bishop.h
- rook.cpp
- rook.h
- newGame.cpp
- newGame.h
- README.md
- .gitignore

## Contributing

If you have any tips, sugestions or things like that feel free to send me a message :speech_balloon:, although I might me slow to respond :sweat_smile:

## License

`UNLICENSED`
