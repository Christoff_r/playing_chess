#include <iostream>
#include "Birb.h"
#include <chrono>
#include <thread>

using namespace std;
using namespace chrono;
using namespace this_thread;

void birb()
{
    int interval = 50;

    cout << "\033[5;31mTEST\033[0m";
    cout << "\033[5;32mTEST\033[0m";
    cout << "\033[5;33mTEST\033[0m";
    cout << "\033[5;34mTEST\033[0m";
    cout << "\033[5;35mTEST\033[0m";
    cout << "\033[5;36mTEST\033[0m";


    sleep_for(milliseconds(interval));
    // Frame 0
    cout << "\033[2J\033[1;1H";

    cout << "\033[5;31m                         .cccc;;cc;';c.           \033[0m" << endl;
    cout << "\033[5;31m                      .,:dkdc:;;:c:,:d:.          \033[0m" << endl;
    cout << "\033[5;31m                     .loc'.,cc::c:::,..;:.        \033[0m" << endl;
    cout << "\033[5;31m                   .cl;....;dkdccc::,...c;        \033[0m" << endl;
    cout << "\033[5;31m                  .c:,';:'..ckc',;::;....;c.      \033[0m" << endl;
    cout << "\033[5;31m                .c:'.,dkkoc:ok:;llllc,,c,';:.     \033[0m" << endl;
    cout << "\033[5;31m               .;c,';okkkkkkkk:;lllll,:kd;.;:,.   \033[0m" << endl;
    cout << "\033[5;31m               co..:kkkkkkkkkk:;llllc':kkc..oNc   \033[0m" << endl;
    cout << "\033[5;31m             .cl;.,oxkkkkkkkkkc,:cll;,okkc'.cO;   \033[0m" << endl;
    cout << "\033[5;31m             ;k:..ckkkkkkkkkkkl..,;,.;xkko:',l'   \033[0m" << endl;
    cout << "\033[5;31m            .,...';dkkkkkkkkkkd;.....ckkkl'.cO;   \033[0m" << endl;
    cout << "\033[5;31m         .,,:,.;oo:ckkkkkkkkkkkdoc;;cdkkkc..cd,   \033[0m" << endl;
    cout << "\033[5;31m      .cclo;,ccdkkl;llccdkkkkkkkkkkkkkkkd,.c;     \033[0m" << endl;
    cout << "\033[5;31m     .lol:;;okkkkkxooc::coodkkkkkkkkkkkko'.oc     \033[0m" << endl;
    cout << "\033[5;31m   .c:'..lkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkd,.oc     \033[0m" << endl;
    cout << "\033[5;31m  .lo;,:cdkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkd,.c;     \033[0m" << endl;
    cout << "\033[5;31m,dx:..;lllllllllllllllllllllllllllllllllc'...     \033[0m" << endl;
    cout << "\033[5;31mcNO;........................................      \033[0m" << endl;

    sleep_for(milliseconds(interval));
    // Frame 1

    cout << "\033[2J\033[1;1H";

    cout << "\033[5;32m                .ckx;'........':c.                \033[0m" << endl;
    cout << "\033[5;32m             .,:c:::::oxxocoo::::,',.             \033[0m" << endl;
    cout << "\033[5;32m            .odc'..:lkkoolllllo;..;d,             \033[0m" << endl;
    cout << "\033[5;32m            ;c..:o:..;:..',;'.......;.            \033[0m" << endl;
    cout << "\033[5;32m           ,c..:0Xx::o:.,cllc:,'::,.,c.           \033[0m" << endl;
    cout << "\033[5;32m           ;c;lkXKXXXXl.;lllll;lKXOo;':c.         \033[0m" << endl;
    cout << "\033[5;32m         ,dc.oXXXXXXXXl.,lllll;lXXXXx,c0:         \033[0m" << endl;
    cout << "\033[5;32m         ;Oc.oXXXXXXXXo.':ll:;'oXXXXO;,l'         \033[0m" << endl;
    cout << "\033[5;32m         'l;;kXXXXXXXXd'.'::'..dXXXXO;,l'         \033[0m" << endl;
    cout << "\033[5;32m         'l;:0XXXXXXXX0x:...,:o0XXXXx,:x,         \033[0m" << endl;
    cout << "\033[5;32m         'l;;kXXXXXXXXXKkol;oXXXXXXXO;oNc         \033[0m" << endl;
    cout << "\033[5;32m        ,c'..ckk0XXXXXXXXXX00XXXXXXX0:;o:.        \033[0m" << endl;
    cout << "\033[5;32m      .':;..:do::ooookXXXXXXXXXXXXXXXo..c;        \033[0m" << endl;
    cout << "\033[5;32m    .',',:co0XX0kkkxxOXXXXXXXXXXXXXXXOc..;l.      \033[0m" << endl;
    cout << "\033[5;32m  .:;'..oXXXXXXXXXXXXXXXXXXXXXXXXXXXXXko;';:.     \033[0m" << endl;
    cout << "\033[5;32m.ldc..:oOXKXXXXXXKXXKXXXXXXXXXXXXXXXXXXXo..oc     \033[0m" << endl;
    cout << "\033[5;32m:0o...:dxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxo,.:,     \033[0m" << endl;
    cout << "\033[5;32mcNo........................................;'     \033[0m" << endl;

    sleep_for(milliseconds(interval));
    //Frame 2

    cout << "\033[2J\033[1;1H";

    cout << "\033[5;33m            .cc;.  ...  .;c.                      \033[0m" << endl;
    cout << "\033[5;33m         .,,cc:cc:lxxxl:ccc:;,.                   \033[0m" << endl;
    cout << "\033[5;33m        .lo;...lKKklllookl..cO;                   \033[0m" << endl;
    cout << "\033[5;33m      .cl;.,:'.okl;..''.;,..';:.                  \033[0m" << endl;
    cout << "\033[5;33m     .:o;;dkd,.ll..,cc::,..,'.;:,.                \033[0m" << endl;
    cout << "\033[5;33m     co..lKKKkokl.':lloo;''ol..;dl.               \033[0m" << endl;
    cout << "\033[5;33m   .,c;.,xKKKKKKo.':llll;.'oOxl,.cl,.             \033[0m" << endl;
    cout << "\033[5;33m   cNo..lKKKKKKKo'';llll;;okKKKl..oNc             \033[0m" << endl;
    cout << "\033[5;33m   cNo..lKKKKKKKko;':c:,'lKKKKKo'.oNc             \033[0m" << endl;
    cout << "\033[5;33m   cNo..lKKKKKKKKKl.....'dKKKKKxc,l0:             \033[0m" << endl;
    cout << "\033[5;33m   .c:'.lKKKKKKKKKk;....lKKKKKKo'.oNc             \033[0m" << endl;
    cout << "\033[5;33m     ,:.'oxOKKKKKKKOxxxxOKKKKKKxc,;ol:.           \033[0m" << endl;
    cout << "\033[5;33m     ;c..'':oookKKKKKKKKKKKKKKKKKk:.'clc.         \033[0m" << endl;
    cout << "\033[5;33m   ,xl'.,oxo;'';oxOKKKKKKKKKKKKKKKOxxl:::;,.      \033[0m" << endl;
    cout << "\033[5;33m  .dOc..lKKKkoooookKKKKKKKKKKKKKKKKKKKxl,;ol.     \033[0m" << endl;
    cout << "\033[5;33m  cx,';okKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKl..;lc.   \033[0m" << endl;
    cout << "\033[5;33m  co..:dddddddddddddddddddddddddddddddddl::',::.  \033[0m" << endl;
    cout << "\033[5;33m  co...........................................   \033[0m" << endl;
    

    sleep_for(milliseconds(interval));
    //Frame 3
    
    cout << "\033[2J\033[1;1H";

    cout << "\033[5;34m           .ccccccc.                              \033[0m" << endl;
    cout << "\033[5;34m      .,,,;cooolccoo;;,,.                         \033[0m" << endl;
    cout << "\033[5;34m     .dOx;..;lllll;..;xOd.                        \033[0m" << endl;
    cout << "\033[5;34m   .cdo;',loOXXXXXkll;';odc.                      \033[0m" << endl;
    cout << "\033[5;34m  ,ol:;c,':oko:cccccc,...ckl.                     \033[0m" << endl;
    cout << "\033[5;34m  ;c.;kXo..::..;c::'.......oc                     \033[0m" << endl;
    cout << "\033[5;34m,dc..oXX0kk0o.':lll;..cxxc.,ld,                   \033[0m" << endl;
    cout << "\033[5;34mkNo.'oXXXXXXo',:lll;..oXXOo;cOd.                  \033[0m" << endl;
    cout << "\033[5;34mKOc;oOXXXXXXo.':lol;..dXXXXl';xc                  \033[0m" << endl;
    cout << "\033[5;34mOl,:k0XXXXXX0c.,clc'.:0XXXXx,.oc                  \033[0m" << endl;
    cout << "\033[5;34mKOc;dOXXXXXXXl..';'..lXXXXXo..oc                  \033[0m" << endl;
    cout << "\033[5;34mdNo..oXXXXXXXOx:..'lxOXXXXXk,.:; ..               \033[0m" << endl;
    cout << "\033[5;34mcNo..lXXXXXXXXXOolkXXXXXXXXXkl,..;:';.            \033[0m" << endl;
    cout << "\033[5;34m.,;'.,dkkkkk0XXXXXXXXXXXXXXXXXOxxl;,;,;l:.        \033[0m" << endl;
    cout << "\033[5;34m  ;c.;:''''':doOXXXXXXXXXXXXXXXXXXOdo;';clc.      \033[0m" << endl;
    cout << "\033[5;34m  ;c.lOdood:'''oXXXXXXXXXXXXXXXXXXXXXk,..;ol.     \033[0m" << endl;
    cout << "\033[5;34m  ';.:xxxxxocccoxxxxxxxxxxxxxxxxxxxxxxl::'.';;.   \033[0m" << endl;
    cout << "\033[5;34m  ';........................................;l'   \033[0m" << endl;
    

    sleep_for(milliseconds(interval));
    //Frame 4

    cout << "\033[2J\033[1;1H";

    cout << "\033[5;35m                                                  \033[0m" << endl;
    cout << "\033[5;35m        .;:;;,.,;;::,.                            \033[0m" << endl;
    cout << "\033[5;35m     .;':;........'co:.                           \033[0m" << endl;
    cout << "\033[5;35m   .clc;'':cllllc::,.':c.                         \033[0m" << endl;
    cout << "\033[5;35m  .lo;;o:coxdllllllc;''::,,.                      \033[0m" << endl;
    cout << "\033[5;35m.c:'.,cl,.'l:',,;;'......cO;                      \033[0m" << endl;
    cout << "\033[5;35mdo;';oxoc;:l;;llllc'.';;'.,;.                     \033[0m" << endl;
    cout << "\033[5;35mc..ckkkkkkkd,;llllc'.:kkd;.':c.                   \033[0m" << endl;
    cout << "\033[5;35m'.,okkkkkkkkc;lllll,.:kkkdl,cO;                   \033[0m" << endl;
    cout << "\033[5;35m..;xkkkkkkkkc,ccll:,;okkkkk:,co,                  \033[0m" << endl;
    cout << "\033[5;35m..,dkkkkkkkkc..,;,'ckkkkkkkc;ll.                  \033[0m" << endl;
    cout << "\033[5;35m..'okkkkkkkko,....'okkkkkkkc,:c.                  \033[0m" << endl;
    cout << "\033[5;35mc..ckkkkkkkkkdl;,:okkkkkkkkd,.',';.               \033[0m" << endl;
    cout << "\033[5;35md..':lxkkkkkkkkxxkkkkkkkkkkkdoc;,;'..'.,.         \033[0m" << endl;
    cout << "\033[5;35mo...'';llllldkkkkkkkkkkkkkkkkkkdll;..'cdo.        \033[0m" << endl;
    cout << "\033[5;35mo..,l;'''''';dkkkkkkkkkkkkkkkkkkkkdlc,..;lc.      \033[0m" << endl;
    cout << "\033[5;35mo..;lc;;;;;;,,;clllllllllllllllllllllc'..,:c.     \033[0m" << endl;
    cout << "\033[5;35mo..........................................;'     \033[0m" << endl;


    sleep_for(milliseconds(interval));
    //Frame 5

    cout << "\033[2J\033[1;1H";

    cout << "\033[5;36m                                                  \033[0m" << endl;
    cout << "\033[5;36m           .,,,,,,,,,.                            \033[0m" << endl;
    cout << "\033[5;36m         .ckKxodooxOOdcc.                         \033[0m" << endl;
    cout << "\033[5;36m      .cclooc'....';;cool.                        \033[0m" << endl;
    cout << "\033[5;36m     .loc;;;;clllllc;;;;;:;,.                     \033[0m" << endl;
    cout << "\033[5;36m   .c:'.,okd;;cdo:::::cl,..oc                     \033[0m" << endl;
    cout << "\033[5;36m  .:o;';okkx;';;,';::;'....,:,.                   \033[0m" << endl;
    cout << "\033[5;36m  co..ckkkkkddkc,cclll;.,c:,:o:.                  \033[0m" << endl;
    cout << "\033[5;36m  co..ckkkkkkkk:,cllll;.:kkd,.':c.                \033[0m" << endl;
    cout << "\033[5;36m.,:;.,okkkkkkkk:,cclll;.ckkkdl;;o:.               \033[0m" << endl;
    cout << "\033[5;36mcNo..ckkkkkkkkko,.;loc,.ckkkkkc..oc               \033[0m" << endl;
    cout << "\033[5;36m,dd;.:kkkkkkkkkx;..;:,.'lkkkkko,.:,               \033[0m" << endl;
    cout << "\033[5;36m  ;:.ckkkkkkkkkkc.....;ldkkkkkk:.,'               \033[0m" << endl;
    cout << "\033[5;36m,dc..'okkkkkkkkkxoc;;cxkkkkkkkkc..,;,.            \033[0m" << endl;
    cout << "\033[5;36mkNo..':lllllldkkkkkkkkkkkkkkkkkdcc,.;l.           \033[0m" << endl;
    cout << "\033[5;36mKOc,c;''''''';lldkkkkkkkkkkkkkkkkkc..;lc.         \033[0m" << endl;
    cout << "\033[5;36mxx:':;;;;,.,,...,;;cllllllllllllllc;'.;od,        \033[0m" << endl;
    cout << "\033[5;36mcNo.....................................oc        \033[0m" << endl;
    

    sleep_for(milliseconds(interval));
    //Frame 6
    
    cout << "\033[2J\033[1;1H";

    cout << "\033[5;31m                                                  \033[0m" << endl;
    cout << "\033[5;31m                                                  \033[0m" << endl;
    cout << "\033[5;31m                   .ccccccc.                      \033[0m" << endl;
    cout << "\033[5;31m               .ccckNKOOOOkdcc.                   \033[0m" << endl;
    cout << "\033[5;31m            .;;cc:ccccccc:,:c::,,.                \033[0m" << endl;
    cout << "\033[5;31m         .c;:;.,cccllxOOOxlllc,;ol.               \033[0m" << endl;
    cout << "\033[5;31m        .lkc,coxo:;oOOxooooooo;..:,               \033[0m" << endl;
    cout << "\033[5;31m      .cdc.,dOOOc..cOd,.',,;'....':l.             \033[0m" << endl;
    cout << "\033[5;31m      cNx'.lOOOOxlldOc..;lll;.....cO;             \033[0m" << endl;
    cout << "\033[5;31m     ,do;,:dOOOOOOOOOl'':lll;..:d:''c,            \033[0m" << endl;
    cout << "\033[5;31m     co..lOOOOOOOOOOOl'':lll;.'lOd,.cd.           \033[0m" << endl;
    cout << "\033[5;31m     co.'dOOOOOOOOOOOo,.;llc,.,dOOc..dc           \033[0m" << endl;
    cout << "\033[5;31m     co..lOOOOOOOOOOOOc.';:,..cOOOl..oc           \033[0m" << endl;
    cout << "\033[5;31m   .,:;.'::lxOOOOOOOOOo:'...,:oOOOc.'dc           \033[0m" << endl;
    cout << "\033[5;31m   ;Oc..cl'':lldOOOOOOOOdcclxOOOOx,.cd.           \033[0m" << endl;
    cout << "\033[5;31m  .:;';lxl''''':lldOOOOOOOOOOOOOOc..oc            \033[0m" << endl;
    cout << "\033[5;31m,dl,.'cooc:::,....,::coooooooooooc'.c:            \033[0m" << endl;
    cout << "\033[5;31mcNo.................................oc            \033[0m" << endl;
    

    sleep_for(milliseconds(interval));
    //Frame 7
    
    cout << "\033[2J\033[1;1H";
    
    cout << "\033[5;32m                                                  \033[0m" << endl;
    cout << "\033[5;32m                                                  \033[0m" << endl;
    cout << "\033[5;32m                                                  \033[0m" << endl;
    cout << "\033[5;32m                        .cccccccc.                \033[0m" << endl;
    cout << "\033[5;32m                  .,,,;;cc:cccccc:;;,.            \033[0m" << endl;
    cout << "\033[5;32m                .cdxo;..,::cccc::,..;l.           \033[0m" << endl;
    cout << "\033[5;32m               ,do:,,:c:coxxdllll:;,';:,.         \033[0m" << endl;
    cout << "\033[5;32m             .cl;.,oxxc'.,cc,.';;;'...oNc         \033[0m" << endl;
    cout << "\033[5;32m             ;Oc..cxxxc'.,c;..;lll;...cO;         \033[0m" << endl;
    cout << "\033[5;32m           .;;',:ldxxxdoldxc..;lll:'...'c,        \033[0m" << endl;
    cout << "\033[5;32m           ;c..cxxxxkxxkxxxc'.;lll:'','.cdc.      \033[0m" << endl;
    cout << "\033[5;32m         .c;.;odxxxxxxxxxxxd;.,cll;.,l:.'dNc      \033[0m" << endl;
    cout << "\033[5;32m        .:,''ccoxkxxkxxxxxxx:..,:;'.:xc..oNc      \033[0m" << endl;
    cout << "\033[5;32m      .lc,.'lc':dxxxkxxxxxxxol,...',lx:..dNc      \033[0m" << endl;
    cout << "\033[5;32m     .:,',coxoc;;ccccoxxxxxxxxo:::oxxo,.cdc.      \033[0m" << endl;
    cout << "\033[5;32m  .;':;.'oxxxxxc''''';cccoxxxxxxxxxxxc..oc        \033[0m" << endl;
    cout << "\033[5;32m,do:'..,:llllll:;;;;;;,..,;:lllllllll;..oc        \033[0m" << endl;
    cout << "\033[5;32mcNo.....................................oc        \033[0m" << endl;
    

    sleep_for(milliseconds(interval));
    //Frame 8
    
    cout << "\033[2J\033[1;1H";

    cout << "\033[5;33m                                                  \033[0m" << endl;
    cout << "\033[5;33m                                                  \033[0m" << endl;
    cout << "\033[5;33m                              .ccccc.             \033[0m" << endl;
    cout << "\033[5;33m                         .cc;'coooxkl;.           \033[0m" << endl;
    cout << "\033[5;33m                     .:c:::c:,,,,,;c;;,.'.        \033[0m" << endl;
    cout << "\033[5;33m                   .clc,',:,..:xxocc;'..c;        \033[0m" << endl;
    cout << "\033[5;33m                  .c:,';:ox:..:c,,,,,,...cd,      \033[0m" << endl;
    cout << "\033[5;33m                .c:'.,oxxxxl::l:.,loll;..;ol.     \033[0m" << endl;
    cout << "\033[5;33m                ;Oc..:xxxxxxxxx:.,llll,....oc     \033[0m" << endl;
    cout << "\033[5;33m             .,;,',:loxxxxxxxxx:.,llll;.,,.'ld,   \033[0m" << endl;
    cout << "\033[5;33m            .lo;..:xxxxxxxxxxxx:.'cllc,.:l:'cO;   \033[0m" << endl;
    cout << "\033[5;33m           .:;...'cxxxxxxxxxxxxoc;,::,..cdl;;l'   \033[0m" << endl;
    cout << "\033[5;33m         .cl;':,'';oxxxxxxdxxxxxx:....,cooc,cO;   \033[0m" << endl;
    cout << "\033[5;33m     .,,,::;,lxoc:,,:lxxxxxxxxxxxo:,,;lxxl;'oNc   \033[0m" << endl;
    cout << "\033[5;33m   .cdxo;':lxxxxxxc'';cccccoxxxxxxxxxxxxo,.;lc.   \033[0m" << endl;
    cout << "\033[5;33m  .loc'.'lxxxxxxxxocc;''''';ccoxxxxxxxxx:..oc     \033[0m" << endl;
    cout << "\033[5;33molc,..',:cccccccccccc:;;;;;;;;:ccccccccc,.'c,     \033[0m" << endl;
    cout << "\033[5;33mOl;......................................;l'      \033[0m" << endl;
    

    sleep_for(milliseconds(interval));
    //Frame 9
    
    cout << "\033[2J\033[1;1H";
    
    cout << "\033[5;34m                                                  \033[0m" << endl;
    cout << "\033[5;34m                              ,ddoodd,            \033[0m" << endl;
    cout << "\033[5;34m                         .cc' ,ooccoo,'cc.        \033[0m" << endl;
    cout << "\033[5;34m                      .ccldo;...',,...;oxdc.      \033[0m" << endl;
    cout << "\033[5;34m                   .,,:cc;.,'..;lol;;,'..lkl.     \033[0m" << endl;
    cout << "\033[5;34m                  .dOc';:ccl;..;dl,.''.....oc     \033[0m" << endl;
    cout << "\033[5;34m                .,lc',cdddddlccld;.,;c::'..,cc:.  \033[0m" << endl;
    cout << "\033[5;34m                cNo..:ddddddddddd;':clll;,c,';xc  \033[0m" << endl;
    cout << "\033[5;34m               .lo;,clddddddddddd;':clll;:kc..;'  \033[0m" << endl;
    cout << "\033[5;34m             .,c;..:ddddddddddddd:';clll,;ll,..   \033[0m" << endl;
    cout << "\033[5;34m             ;Oc..';:ldddddddddddl,.,c:;';dd;..   \033[0m" << endl;
    cout << "\033[5;34m           .''',:c:,'cdddddddddddo:,''..'cdd;..   \033[0m" << endl;
    cout << "\033[5;34m         .cdc';lddd:';lddddddddddddd;.';lddl,..   \033[0m" << endl;
    cout << "\033[5;34m      .,;::;,cdddddol;;lllllodddddddlcldddd:.'l;  \033[0m" << endl;
    cout << "\033[5;34m     .dOc..,lddddddddlcc:;'';cclddddddddddd;;ll.  \033[0m" << endl;
    cout << "\033[5;34m   .coc,;::ldddddddddddddlcccc:ldddddddddl:,cO;   \033[0m" << endl;
    cout << "\033[5;34m,xl::,..,cccccccccccccccccccccccccccccccc:;':xx,  \033[0m" << endl;
    cout << "\033[5;34mcNd.........................................;lOc  \033[0m" << endl;


    birb();



}