#include <iostream>
#include "newGame.h"
#include "chessBoard.h"
#include "gameState.h"

#include "chessman.h"
#include "pawn.h"
#include "rook.h"
#include "knight.h"
#include "bishop.h"
#include "queen.h"
#include "king.h"

using namespace std;

void NewGame()
{

// White Team

// Line1
Rook rook1(1, pair<int, int>(0, 0), true, "♜", "Rook");
Knight knight1(2, pair<int, int>(0, 1), true, "♞", "Knight");
Bishop bishop1(3, pair<int, int>(0, 2), true, "♝", "Bishop");
Queen queen1(4, pair<int, int>(0, 3), true, "♛", "Queen");
King king1(5, pair<int, int>(0, 4), true, "♚", "King");
Bishop bishop2(6, pair<int, int>(0, 5), true, "♝", "Bishop");
Knight knight2(7, pair<int, int>(0, 6), true, "♞", "Knight");
Rook rook2(8, pair<int, int>(0, 7), true, "♜", "Rook");

// Line2
Pawn pawn1(9, pair<int, int>(1, 0), true, "♟", "Pawn");
Pawn pawn2(10, pair<int, int>(1, 1), true, "♟", "Pawn");
Pawn pawn3(11, pair<int, int>(1, 2), true, "♟", "Pawn");
Pawn pawn4(12, pair<int, int>(1, 3), true, "♟", "Pawn");
Pawn pawn5(13, pair<int, int>(1, 4), true, "♟", "Pawn");
Pawn pawn6(14, pair<int, int>(1, 5), true, "♟", "Pawn");
Pawn pawn7(15, pair<int, int>(1, 6), true, "♟", "Pawn");
Pawn pawn8(16, pair<int, int>(1, 7), true, "♟", "Pawn");

// Black Team

// Line1
Rook rook3(17, pair<int, int>(7, 0), false, "♖", "Rook");
Knight knight3(18, pair<int, int>(7, 1), false, "♘", "Knight");
Bishop bishop3(19, pair<int, int>(7, 2), false, "♗", "Bishop");
Queen queen2(20, pair<int, int>(7, 3), false, "♕", "Queen");
King king2(21, pair<int, int>(7, 4), false, "♔", "King");
Bishop bishop4(22, pair<int, int>(7, 5), false, "♗", "Bishop");
Knight knight4(23, pair<int, int>(7, 6), false, "♘", "Knight");
Rook rook4(24, pair<int, int>(7, 7), false, "♖", "Rook");

// Line2
Pawn pawn9(25, pair<int, int>(6, 0), false, "♙", "Pawn");
Pawn pawn10(26, pair<int, int>(6, 1), false, "♙", "Pawn");
Pawn pawn11(27, pair<int, int>(6, 2), false, "♙", "Pawn");
Pawn pawn12(28, pair<int, int>(6, 3), false, "♙", "Pawn");
Pawn pawn13(29, pair<int, int>(6, 4), false, "♙", "Pawn");
Pawn pawn14(30, pair<int, int>(6, 5), false, "♙", "Pawn");
Pawn pawn15(31, pair<int, int>(6, 6), false, "♙", "Pawn");
Pawn pawn16(32, pair<int, int>(6, 7), false, "♙", "Pawn");

// Pieces start at top and bottom
int freshBoardID[8][8] =
    {
        {{rook1.GetID()}, {knight1.GetID()}, {bishop1.GetID()}, {queen1.GetID()}, {king1.GetID()}, {bishop2.GetID()}, {knight2.GetID()}, {rook2.GetID()}},
        {{pawn1.GetID()}, {pawn2.GetID()}, {pawn3.GetID()}, {pawn4.GetID()}, {pawn5.GetID()}, {pawn6.GetID()}, {pawn7.GetID()}, {pawn8.GetID()}},
        {{-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1}},
        {{-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1}},
        {{-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1}},
        {{-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1}},
        {{rook3.GetID()}, {knight3.GetID()}, {bishop3.GetID()}, {queen2.GetID()}, {king2.GetID()}, {bishop4.GetID()}, {knight4.GetID()}, {rook4.GetID()}},
        {{pawn9.GetID()}, {pawn10.GetID()}, {pawn11.GetID()}, {pawn12.GetID()}, {pawn13.GetID()}, {pawn14.GetID()}, {pawn15.GetID()}, {pawn16.GetID()}}

    };
    
currentBoardID[8][8] = freshBoardID[8][8];

chessmenInPlay = {&rook1, &knight1, &bishop1, &queen1, &king1, &bishop2, &knight2, &rook2,
                    &pawn1, &pawn2, &pawn3, &pawn4, &pawn5, &pawn6, &pawn7, &pawn8,
                    &pawn9, &pawn10, &pawn11, &pawn12, &pawn13, &pawn14, &pawn15, &pawn16,
                    &rook3, &knight3, &bishop3, &queen2, &king2, &bishop4, &knight4, &rook4};

// Update the gamestate board of IDs
updateChessBoard(freshBoardID);
// Draw the game board
drawChessBoard();
}


/*
║♜│♞│♝│♛│♚│♝│♞│♜║
║♟│♟│♟│♟│♟│♟│♟│♟║




║♙│♙│♙│♙│♙│♙│♙│♙║
║♖│♘│♗│♕│♔│♗│♘│♖║
*/
