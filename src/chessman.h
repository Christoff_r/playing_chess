#pragma once 
#include <iostream>
#include <string>
using namespace std;

class Chessman
{
    protected:
        int id; //For computaional work only
        pair<int,int> cordinates;
        bool teamWhite;
        string logo = "| Å |";
        string name = "Im not supposed to have a name";
        
    public:
        Chessman(int id, pair<int,int> cordinates, bool teamWhite, string logo, string name);
        virtual void move(pair<int,int> moveToPos);
        virtual void checkMove(pair<int,int> checkPos);
        void assignLogo(string logo);
        string GetLogo();
        void assignName(string logo);
        void assignCordinates(pair<int,int> cordinates);
        pair<int,int> GetCordniates();
        int GetID();
        bool GetTeamWhite();
};