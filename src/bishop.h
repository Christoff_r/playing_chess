#pragma once
#include <iostream>
#include <string>
#include "chessman.h"
using namespace std;

class Bishop : public Chessman
{
    protected:

    public:
    Bishop(int id, pair<int,int> cordinates, bool teamWhite, string logo, string name);
    void move(pair<int, int> moveToPos);
    void checkMove(pair<int,int> checkPos);
};