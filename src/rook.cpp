#include "rook.h"
#include "inputHandler.h"
#include "chessBoard.h"
#include "gameState.h"
#include <string>
using namespace std;

Rook::Rook(int id, pair<int, int> cordinates, bool teamWhite, string logo, string name) : Chessman(id, cordinates, teamWhite, logo, name)
{
    this->id = id;
    this->cordinates = cordinates;
    this->teamWhite = teamWhite;
    this->logo = logo;
    this->name = name;
}
void Rook::move(pair<int, int> moveToPos)
{
    /*
    if (cordinates.first == moveToPos.first || cordinates.second == moveToPos.second)
    {
#pragma region Check Location For Piece
        // Checking the targeted location for what is there
        bool pieceToKillAtPos;

        // There is NOT a piece at the location
        if (currentBoardID[moveToPos.first][moveToPos.second] == -1)
        {
            pieceToKillAtPos = false;
        }
        // There is a piece at the location
        else if (currentBoardID[moveToPos.first][moveToPos.second] != -1)
        {
            // is it an ally of this piece or not?
            for (int i = 0; i < chessmenInPlay.size(); i++)
            {
                // its NOT an allied piece
                if (chessmenInPlay[i]->GetID() == currentBoardID[moveToPos.first][moveToPos.second] && chessmenInPlay[i]->GetTeamWhite() != teamWhite)
                {
                    bool pieceToKillAtPos = true;
                }
                // its an allied piece
                else if (chessmenInPlay[i]->GetID() == currentBoardID[moveToPos.first][moveToPos.second] && chessmenInPlay[i]->GetTeamWhite() == teamWhite)
                {
                    inputHandler("Invalid Move: User tried to attack an allied piece");
                }
            }
        }
   
    int å = 0;
if (cordinates.first + 1 == moveToPos.first && cordinates.second == moveToPos.second || cordinates.second + 1 == moveToPos.second && cordinates.first == moveToPos.first || cordinates.first - 1 == moveToPos.first && cordinates.second == moveToPos.second || cordinates.second - 1 == moveToPos.second && cordinates.first == moveToPos.first)
    {
        å = 0;
    }
    else
    {
        å = 1;
    }

#pragma endregion

#pragma region Checks Straight Line Under the tower

        // Straight line UNDER the tower
        if (cordinates.first < moveToPos.first && cordinates.second == moveToPos.second)
        {
            // Checks all places except the final position for other pieces
            for (int i = cordinates.first + 1; i <= moveToPos.first - å; i++) //TODO: remove the +- 1 in the last part on the last 3
            {
                // If it finds a single piece on the way we exit back to inputhandler
                if (currentBoardID[i][cordinates.second] != -1)
                {
                    inputHandler("Can't move down. Somethig in the way");
                }
                // If we havent found any pieces in the way
                else if (i == moveToPos.first - å) // TODO: Remove the +- 1 here in the other 3
                {
                    // We kill the opponent and stand in his spot
                    if (pieceToKillAtPos == true)
                    {
                        // Finds and removes the opponent from the game
                        for (int i = 0; i < chessmenInPlay.size(); i++)
                        {
                            if (chessmenInPlay[i]->GetID() == currentBoardID[moveToPos.first][moveToPos.second])
                            {
                                chessmenInPlay.erase(chessmenInPlay.begin() + i);
                            }
                        }

                        // Sets our old ID position to empty (-1)
                        currentBoardID[cordinates.first][cordinates.second] = -1;
                        // Change our coords to the new position
                        cordinates = moveToPos;
                        // Updates the IDBoard with our ID
                        currentBoardID[moveToPos.first][moveToPos.second] = id;
                        cout << "We successfully exited to end turn line 100" << endl;
                        endTurn();
                    }
                    // We move peacefully to the position
                    else
                    {
                        cout << "we move to the position without killing line 106" << endl;
                        // Sets our old ID position to empty (-1)
                        currentBoardID[cordinates.first][cordinates.second] = -1;
                        // Change our coords to the new position
                        cordinates = moveToPos;
                        // Updates the IDBoard with our ID
                        currentBoardID[moveToPos.first][moveToPos.second] = id;
                        cout << "we successfully exited to end turn line 113" << endl;
                        endTurn();
                    }
                }
            }
        }
#pragma endregion

#pragma region Checks Straight Line Above the tower

        // Straight line OVER the tower
        else if (cordinates.first > moveToPos.first && cordinates.second == moveToPos.second)
        {
            // Checks all places except the final position for other pieces
            for (int i = cordinates.first - 1; i >= moveToPos.first + å; i--)
            {
                // If it finds a single piece on the way we exit back to inputhandler
                if (currentBoardID[i][cordinates.second] != -1)
                {
                    inputHandler("Can't move up. Something in the way");
                }
                // If we havent found any pieces in the way
                else if (i == moveToPos.first + å)
                {
                    // We kill the opponent and stand in his spot
                    if (pieceToKillAtPos == true)
                    {
                        // Finds and removes the opponent from the game
                        for (int i = 0; i < chessmenInPlay.size(); i++)
                        {
                            if (chessmenInPlay[i]->GetID() == currentBoardID[moveToPos.first][moveToPos.second])
                            {
                                chessmenInPlay.erase(chessmenInPlay.begin() + i);
                            }
                        }

                        // Sets our old ID position to empty (-1)
                        currentBoardID[cordinates.first][cordinates.second] = -1;
                        // Change our coords to the new position
                        cordinates = moveToPos;
                        // Updates the IDBoard with our ID
                        currentBoardID[moveToPos.first][moveToPos.second] = id;

                        endTurn();
                    }
                    // We move peacefully to the position
                    else
                    {
                        // Sets our old ID position to empty (-1)
                        currentBoardID[cordinates.first][cordinates.second] = -1;
                        // Change our coords to the new position
                        cordinates = moveToPos;
                        // Updates the IDBoard with our ID
                        currentBoardID[moveToPos.first][moveToPos.second] = id;

                        endTurn();
                    }
                }
            }
        }
#pragma endregion

#pragma region Checks Straight Line Right for tower
        // Straight line RIGHT of the tower
        else if (cordinates.second < moveToPos.second && cordinates.first == moveToPos.first)
        {
            for (int i = cordinates.second + 1; i <= moveToPos.second - å; i++)
            {
                // If it finds a single piece on the way we exit back to inputhandler
                if (currentBoardID[i][cordinates.second] != -1)
                {
                    inputHandler("Can't move right. Something in the way");
                }
                // If we havent found any pieces in the way
                else if (i == moveToPos.second - å)
                {
                    // We kill the opponent and stand in his spot
                    if (pieceToKillAtPos == true)
                    {
                        // Finds and removes the opponent from the game
                        for (int i = 0; i < chessmenInPlay.size(); i++)
                        {
                            if (chessmenInPlay[i]->GetID() == currentBoardID[moveToPos.first][moveToPos.second])
                            {
                                chessmenInPlay.erase(chessmenInPlay.begin() + i);
                            }
                        }

                        // Sets our old ID position to empty (-1)
                        currentBoardID[cordinates.first][cordinates.second] = -1;
                        // Change our coords to the new position
                        cordinates = moveToPos;
                        // Updates the IDBoard with our ID
                        currentBoardID[moveToPos.first][moveToPos.second] = id;

                        endTurn();
                    }
                    // We move peacefully to the position
                    else
                    {
                        cout << "Line 211" << endl;
                        // Sets our old ID position to empty (-1)
                        currentBoardID[cordinates.first][cordinates.second] = -1;
                        // Change our coords to the new position
                        cordinates = moveToPos;
                        // Updates the IDBoard with our ID
                        currentBoardID[moveToPos.first][moveToPos.second] = id;

                        endTurn();
                    }
                }
            }
        }
#pragma endregion

#pragma region Checks Straight Line Left for tower
        // Straight line LEFT of the tower
        else if (cordinates.second > moveToPos.second && cordinates.first == moveToPos.first)
        {
            cout << cordinates.second  << moveToPos.second << å << endl;
            for (int i = cordinates.second - 1; i >= moveToPos.second + å; i--)
            {
                cout << "Line 228" << endl;
                cout << cordinates.second  << moveToPos.second << å <<  i << endl;

                cout << "i is equal to: " << i << "moveToPos is equal to: " << moveToPos.second << endl;
                // If it finds a single piece on the way we exit back to inputhandler
                if (currentBoardID[i][moveToPos.second] != -1)
                {
                    cout << currentBoardID[i][cordinates.second] << endl;
                    inputHandler("Can't move left. Something in the way");
                }
                // If we havent found any pieces in the way
                else if (i == moveToPos.second + å)
                {
                    cout << "Line 238" << endl;
                    // We kill the opponent and stand in his spot
                    if (pieceToKillAtPos == true)
                    {
                        // Finds and removes the opponent from the game
                        for (int i = 0; i < chessmenInPlay.size(); i++)
                        {
                            if (chessmenInPlay[i]->GetID() == currentBoardID[moveToPos.first][moveToPos.second])
                            {
                                chessmenInPlay.erase(chessmenInPlay.begin() + i);
                            }
                        }
                        // Sets our old ID position to empty (-1)
                        currentBoardID[cordinates.first][cordinates.second] = -1;
                        // Change our coords to the new position
                        cordinates = moveToPos;
                        // Updates the IDBoard with our ID
                        currentBoardID[moveToPos.first][moveToPos.second] = id;

                        endTurn();
                    }
                    // We move peacefully to the position
                    else
                    {
                        cout << "Line 262" << endl;
                        // Sets our old ID position to empty (-1)
                        currentBoardID[cordinates.first][cordinates.second] = -1;
                        // Change our coords to the new position
                        cordinates = moveToPos;
                        // Updates the IDBoard with our ID
                        currentBoardID[moveToPos.first][moveToPos.second] = id;

                        endTurn();
                    }
                }
            }
        }
#pragma endregion

#pragma region Invalid Move Call
        else
        {
            cout << "It was the else statement" << endl;
            inputHandler("Illegal Move: the user wanted the tower to move to its own position");
        }
#pragma endregion
    }*/
    bool pieceToKillAtPos;

    // There is NOT a piece at the location
    if (currentBoardID[moveToPos.first][moveToPos.second] == -1)
    {
        pieceToKillAtPos = false;
    }
    // There is a piece at the location
    else if (currentBoardID[moveToPos.first][moveToPos.second] != -1)
    {
        // is it an ally of this piece or not?
        for (int i = 0; i < chessmenInPlay.size(); i++)
        {
            // its NOT an allied piece
            if (chessmenInPlay[i]->GetID() == currentBoardID[moveToPos.first][moveToPos.second] && chessmenInPlay[i]->GetTeamWhite() != teamWhite)
            {
                bool pieceToKillAtPos = true;
            }
            // its an allied piece
            else if (chessmenInPlay[i]->GetID() == currentBoardID[moveToPos.first][moveToPos.second] && chessmenInPlay[i]->GetTeamWhite() == teamWhite)
            {
                inputHandler("Invalid Move: User tried to attack an allied piece");
            }
        }
    }

    // We kill the opponent and stand in his spot
    if (pieceToKillAtPos == true)
    {
        // Finds and removes the opponent from the game
        for (int i = 0; i < chessmenInPlay.size(); i++)
        {
            if (chessmenInPlay[i]->GetID() == currentBoardID[moveToPos.first][moveToPos.second])
            {
                chessmenInPlay.erase(chessmenInPlay.begin() + i);
            }
        }

        // Sets our old ID position to empty (-1)
        currentBoardID[cordinates.first][cordinates.second] = -1;
        // Change our coords to the new position
        cordinates = moveToPos;
        // Updates the IDBoard with our ID
        currentBoardID[moveToPos.first][moveToPos.second] = id;
        endTurn();
    }
    // We move peacefully to the position
    else
    {
        // Sets our old ID position to empty (-1)
        currentBoardID[cordinates.first][cordinates.second] = -1;
        // Change our coords to the new position
        cordinates = moveToPos;
        // Updates the IDBoard with our ID
        currentBoardID[moveToPos.first][moveToPos.second] = id;
        endTurn();
    }
}
void Rook::checkMove(pair<int, int> checkPos)
{
    cout << "checkMove() not yet implemented" << endl;
}
