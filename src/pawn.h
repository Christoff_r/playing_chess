#pragma once
#include <iostream>
#include <string>
#include "chessman.h"
using namespace std;

class Pawn : public Chessman
{
protected:
    bool firstMove = true;

public:
    Pawn(int id, pair<int, int> cordinates, bool teamWhite, string logo, string name);
    void move(pair<int, int> moveToPos);

    void setfirstMove(bool firstMove);
    bool getfirstMove();
    void checkMove(pair<int,int> checkPos);
};