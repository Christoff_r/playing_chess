#include <iostream>
#include <limits>    // To get the numeric_limits
#include <algorithm> // To use the for_each
#include "inputHandler.h"
#include "userCommands.h"
#include "chessBoard.h"
#include "Birb.h"

string input;
int mySize = 7;
string userCommands[7] = {"help", "new", "load", "save", "ff", "move", "birb"};

void inputHandler(string message)
{

    string input;
     // Makes sure we get the correct input and actually get an input
    while (cout << message << "\n" && !(getline(cin, input)))
    {
        // Clear bad flags
        cin.clear();

        // Discard insane input
        cin.ignore(numeric_limits<streamsize>::max(), '\n');

        cout << "Invalid input you doughnut...\n";
    }

    // Make all char lowercase
    for_each(input.begin(), input.end(), [](char &c)
             { c = ::tolower(c); });

    // Check commands
    if (userCommands[0] == input)
    {
        help(userCommands, mySize);
    }
    else if (userCommands[1] == input)
    {    
        newGame();
    }
    else if (userCommands[2] == input)
    {
        load();
    }
    else if (userCommands[3] == input)
    {
        save();
    }
    else if (userCommands[4] == input)
    {
        concede();
    }
    
    // Check if input was a move command
    else if (userCommands[5] == input)
    {
        cout << "Move" << endl;
        pair<int, int> cordinates;
        pair<int, int> moveToPos;
  
        cout << "Enter cordinates..\n";
        cin >> cordinates.first >> cordinates.second >> moveToPos.first >> moveToPos.second;   

        move(cordinates, moveToPos);
    }
    else if (userCommands[6] == input)
    {
        birb();
    }
    else
    {
        inputHandler("Invalid input \n Please type (help) for a list of commands...\n");
    }
}