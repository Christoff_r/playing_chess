#include "king.h"
#include "inputHandler.h"
#include "chessBoard.h"
#include "gameState.h"
#include "chessBoard.h"
#include <string>
using namespace std;

King::King(int id, pair<int, int> cordinates, bool teamWhite, string logo, string name):Chessman(id, cordinates, teamWhite, logo, name)
{
    this->id = id;
    this->cordinates = cordinates;
    this->teamWhite = teamWhite;
    this->logo = logo;
    this->name = name;
}
void King::move(pair<int, int> moveToPos)
{

    /*if (moveToPos.first >! cordinates.first + 2 && moveToPos.first <! cordinates.first - 2 && moveToPos.second >! cordinates.second + 2 && moveToPos.second <! cordinates.second - 2 && moveToPos != cordinates)
    {
        //There is an Object in the position and its not friendly
        if (//TODO:  Arraypos(moveToPos) != -1 && Arraypos(moveToPos.getteamWhite() != teamWhite)
        {
            //TODO: Arraypos(moveToPos) = delete
            endTurn();
            cordinates = moveToPos;
        }
        //There is nothing in the position
        else if (// TODO: Arraypos(moveToPos) == -1)
        {
            endTurn();
            cordinates = moveToPos
        }
        else
        {
            inputHandler("Illegal Move: the user wanted to move the king into a valid position but the position was taken by an ally of the king");
        }
    }
    else
    {
        inputHandler("Illegal Move: the user wanted to move the king to a place outside of the range of the king");
    }*/
        bool pieceToKillAtPos;

    // There is NOT a piece at the location
    if (currentBoardID[moveToPos.first][moveToPos.second] == -1)
    {
        pieceToKillAtPos = false;
    }
    // There is a piece at the location
    else if (currentBoardID[moveToPos.first][moveToPos.second] != -1)
    {
        // is it an ally of this piece or not?
        for (int i = 0; i < chessmenInPlay.size(); i++)
        {
            // its NOT an allied piece
            if (chessmenInPlay[i]->GetID() == currentBoardID[moveToPos.first][moveToPos.second] && chessmenInPlay[i]->GetTeamWhite() != teamWhite)
            {
                bool pieceToKillAtPos = true;
            }
            // its an allied piece
            else if (chessmenInPlay[i]->GetID() == currentBoardID[moveToPos.first][moveToPos.second] && chessmenInPlay[i]->GetTeamWhite() == teamWhite)
            {
                inputHandler("Invalid Move: User tried to attack an allied piece");
            }
        }
    }

    // We kill the opponent and stand in his spot
    if (pieceToKillAtPos == true)
    {
        // Finds and removes the opponent from the game
        for (int i = 0; i < chessmenInPlay.size(); i++)
        {
            if (chessmenInPlay[i]->GetID() == currentBoardID[moveToPos.first][moveToPos.second])
            {
                chessmenInPlay.erase(chessmenInPlay.begin() + i);
            }
        }

        // Sets our old ID position to empty (-1)
        currentBoardID[cordinates.first][cordinates.second] = -1;
        // Change our coords to the new position
        cordinates = moveToPos;
        // Updates the IDBoard with our ID
        currentBoardID[moveToPos.first][moveToPos.second] = id;
        endTurn();
    }
    // We move peacefully to the position
    else
    {
        // Sets our old ID position to empty (-1)
        currentBoardID[cordinates.first][cordinates.second] = -1;
        // Change our coords to the new position
        cordinates = moveToPos;
        // Updates the IDBoard with our ID
        currentBoardID[moveToPos.first][moveToPos.second] = id;
        endTurn();
    }
}
void King::checkMove(pair<int,int> checkPos)
{

}