#pragma once
#include <iostream>
#include <string>
#include "chessman.h"
using namespace std;

class Rook : public Chessman
{
protected:
public:
    Rook(int id, pair<int, int> cordinates, bool teamWhite, string logo, string name);
    void move(pair<int, int> moveToPos) override;
    void checkMove(pair<int,int> checkPos);
};