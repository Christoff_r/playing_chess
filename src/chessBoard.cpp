#include "chessBoard.h"
#include "chessman.h"
#include "inputHandler.h"
#include "gameState.h"

void drawChessBoard()
{ 
    for (int i = 0; i < 8; i++)
    {
        for (int j = 0; j < 8; j++)
        {
            if (currentBoardID[i][j] != -1)
            {
                for (auto chessman : chessmenInPlay)
                {
                    pair coords = chessman->GetCordniates();
                    if (i == coords.first && j == coords.second)
                    {
                        cout << chessman->GetLogo() << " ";
                        break;
                    }
                }
            }
            else
            {
                cout << "# ";
            }
        }
        cout << "\n";
    }

    if(isBlacksTurn)
    {
        cout << "It is Black's turn" << endl;
        cin.clear();
        inputHandler("Type a command..");
    }
    else
    {
        cout << "It is White's turn" << endl;
        cin.clear();
        inputHandler("Type a command..");
    };
}

void updateChessBoard(int boardID[8][8])
{
    for (int i = 0; i < 8; i++)
    {
        for (int j = 0; j < 8; j++)
        {
            currentBoardID[i][j] = boardID[i][j];
        }
    }
}

void getPiece(pair<int, int> c, pair<int, int> mtp)
{
    for (auto chessman : chessmenInPlay)
    {
        if (chessman->GetCordniates() == c)
        {
            chessman->move(mtp);

            break;
        }
    }
}

void endTurn()
{
    isBlacksTurn = !isBlacksTurn;
    drawChessBoard();
}