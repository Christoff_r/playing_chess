#include "pawn.h"
#include "inputHandler.h"
#include "chessBoard.h"
#include "inputHandler.h"
#include "chessBoard.h"
#include "gameState.h"
#include <string>
using namespace std;

Pawn::Pawn(int id, pair<int, int> cordinates, bool teamWhite, string logo, string name) : Chessman(id, cordinates, teamWhite, logo, name)
{
    this->id = id;
    this->cordinates = cordinates;
    this->teamWhite = teamWhite;
    this->logo = logo;
    this->name = name;
}

void Pawn::setfirstMove(bool firstMove)
{
    this->firstMove = firstMove;
}
bool Pawn::getfirstMove()
{
    return firstMove;
}

void Pawn::move(pair<int, int> moveToPos)
{

    // WhitePawn
   /* if (teamWhite == true)
    {
        // Normal move && StartMove
        if (cordinates.first + 1 == moveToPos.first && cordinates.second == moveToPos.second && firstMove != true //TODO: && moveToPos in the array is empty || cordinates.first + 2 == moveToPos.first && cordinates.second == moveToPos.second && firstMove == true TODO: && moveToPos in the array is empty)
            ;
        {
            //TODO:  MovePawnTo"moveToPos"

            endTurn();
        }
        // Attack move
        if (cordinates.first + 1 == moveToPos.first && cordinates.second + 1 == moveToPos.second //TODO: && moveToPos in the array is NOT empty || cordinates.first + 1 == moveToPos.first && cordinates.second - 1 == moveToPos.second TODO: && moveToPos in the array is NOT empty)
        {
            //TODO:  array.moveToPos.object = delete;
            cordinates = moveToPos;

            endTurn();
        }
        else
        {
            inputHandler("Illegal Move: the user wanted the Pawn to move to a position out of range of the pawn");
        }
    }

    // BlackPawn
    if (teamWhite == false)
    {
        // Normal move
        if (cordinates.first - 1 == moveToPos.first && cordinates.second == moveToPos.second && firstMove != true //&& moveToPos in the array is empty || cordinates.first - 2 == moveToPos.first && cordinates.second == moveToPos.second && firstMove == true && moveToPos in the array is empty)
            ;
        {
            // TODO: MovePawnTo"moveToPos"

            endTurn();
        }
        // Attack move
        if (cordinates.first - 1 == moveToPos.first && cordinates.second + 1 == moveToPos.second //&& moveToPos in the array is NOT empty || cordinates.first - 1 == moveToPos.first && cordinates.second - 1 == moveToPos.second && moveToPos in the array is NOT empty)
        {
            // TODO: array.moveToPos.object = delete;
            cordinates = moveToPos;

            endTurn();
        }
        else
        {
            inputHandler("Illegal Move: the user wanted the Pawn to move to a position out of range of the pawn");
        }
    }*/
        bool pieceToKillAtPos;

    // There is NOT a piece at the location
    if (currentBoardID[moveToPos.first][moveToPos.second] == -1)
    {
        pieceToKillAtPos = false;
    }
    // There is a piece at the location
    else if (currentBoardID[moveToPos.first][moveToPos.second] != -1)
    {
        // is it an ally of this piece or not?
        for (int i = 0; i < chessmenInPlay.size(); i++)
        {
            // its NOT an allied piece
            if (chessmenInPlay[i]->GetID() == currentBoardID[moveToPos.first][moveToPos.second] && chessmenInPlay[i]->GetTeamWhite() != teamWhite)
            {
                bool pieceToKillAtPos = true;
            }
            // its an allied piece
            else if (chessmenInPlay[i]->GetID() == currentBoardID[moveToPos.first][moveToPos.second] && chessmenInPlay[i]->GetTeamWhite() == teamWhite)
            {
                inputHandler("Invalid Move: User tried to attack an allied piece");
            }
        }
    }

    // We kill the opponent and stand in his spot
    if (pieceToKillAtPos == true)
    {
        // Finds and removes the opponent from the game
        for (int i = 0; i < chessmenInPlay.size(); i++)
        {
            if (chessmenInPlay[i]->GetID() == currentBoardID[moveToPos.first][moveToPos.second])
            {
                chessmenInPlay.erase(chessmenInPlay.begin() + i);
            }
        }

        // Sets our old ID position to empty (-1)
        currentBoardID[cordinates.first][cordinates.second] = -1;
        // Change our coords to the new position
        cordinates = moveToPos;
        // Updates the IDBoard with our ID
        currentBoardID[moveToPos.first][moveToPos.second] = id;
        endTurn();
    }
    // We move peacefully to the position
    else
    {
        // Sets our old ID position to empty (-1)
        currentBoardID[cordinates.first][cordinates.second] = -1;
        // Change our coords to the new position
        cordinates = moveToPos;
        // Updates the IDBoard with our ID
        currentBoardID[moveToPos.first][moveToPos.second] = id;
        endTurn();
    }
}
void Pawn::checkMove(pair<int,int> checkPos)
{

}