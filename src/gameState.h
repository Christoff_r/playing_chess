#pragma once
#include <iostream>
#include <string>
#include <vector>
#include "chessman.h"
using namespace std;

extern int currentBoardID[8][8];
extern vector<Chessman*> chessmenInPlay;
extern bool isBlacksTurn;