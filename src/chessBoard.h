#pragma once 
#include <iostream>
#include <string>
#include <vector>
#include "chessman.h"
using namespace std;

void drawChessBoard();
void updateChessBoard(int boardID[8][8]);
void endTurn();
void getPiece(pair<int, int> c, pair<int, int> mtp);