#pragma once
#include <iostream>
#include <string>
using namespace std;

void help(string userCommands[], int size);
void newGame();
void load();
void save();
void concede();
void move(pair<int, int> c, pair<int, int> mtp);
