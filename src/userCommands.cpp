#include "inputHandler.h"
#include "userCommands.h"
#include "newGame.h"
#include "chessBoard.h"

// Print the avalible commands
void help(string userCommands[], int size)
{
    for (int i = 0; i < size; i++)
    {
        cout << "- "<< userCommands[i] << endl;
    }

    inputHandler("Type something...");
}

// Start a new game
void newGame()
{
    NewGame();
}

// Load a saved game
void load()
{
    cout << "Load Game" << endl;
    inputHandler("Type something...");
}

// Save a game
void save()
{
    cout << "Save Game" << endl;
    inputHandler("Type something...");
}

// Concede a game
void concede()
{
    cout << "I see mommy raised a quitter..." << endl;
}

// Moves a chessman
void move(pair<int, int> c, pair<int, int> mtp)
{
    getPiece(c, mtp);
}