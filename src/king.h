#pragma once
#include <iostream>
#include <string>
#include "chessman.h"
using namespace std;

class King : public Chessman
{
protected:
public:
    King(int id, pair<int, int> cordinates, bool teamWhite, string logo, string name);
    void move(pair<int, int> moveToPos);
    void checkMove(pair<int,int> checkPos);
};