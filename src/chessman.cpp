#include "chessman.h"

Chessman::Chessman(int id, pair<int,int> cordinates, bool teamWhite, string logo, string name)
{
    this -> id = id;
    this -> cordinates = cordinates;
    this -> teamWhite = teamWhite;
    this -> logo = logo;
    this -> name = name;
}

void Chessman::move(pair<int,int> moveToPos)
{
    //BaseClassMove
}

void Chessman::checkMove(pair<int,int> checkPos)
{
    //BaseClassCheckMove
}

bool Chessman::GetTeamWhite()
{
    return teamWhite;
}

void Chessman::assignLogo(string logo)
{
    this -> logo = logo;
}
string Chessman::GetLogo()
{
    return logo;
}
void Chessman::assignName(string name)
{
    this -> name = name;
}
void Chessman::assignCordinates(pair<int, int> cordinates)
{
    this -> cordinates = cordinates;
}
pair<int, int> Chessman::GetCordniates()
{
    return cordinates;
}

int Chessman::GetID()
{
    return id;
}